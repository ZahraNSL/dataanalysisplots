## Introduction
In this project we will try to enhance quality validation of a solution using descriptive plots.
By quality, we mainly mean minimizing following factors :
* make span
* lateness/earliness of due dates
* resource requirement

This evaluation is under affect of few constraints:
* material availability
* resource availability
* Precedence and overlap
* Changeover time


## Installation
Required packages for this project are listed in requirements file. 
By enabling and activating your python virtual environment you can install list of requirements.
Following command line does this process for Windows users.
For more info please refer to this link : 
https://towardsdatascience.com/virtual-environments-104c62d48c54
```
python -m venv venv
venv/Scripts/activate
pip install -r requirements.txt
```
## Provided Functionalities
* Gantt chart of a schedule based on materials
* Gantt chart of a schedule based on resources
  * for further analysis, details of due dates are added to the plot.
* Bar plot of material availability   
