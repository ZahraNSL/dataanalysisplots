import os
from xml.dom import minidom
import pandas as pd
import glob
import plotly.express as px
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import plotly

def taskDetails(xmldoc):
    task_list = xmldoc.getElementsByTagName('task')
    listOfDetails = []
    for rep in range(0, len(task_list)):
        task_ID = task_list[rep].attributes['ID'].value
        wo_ID = task_ID.split("%")[0]
        op_ID = task_ID.split("%")[1]
        start_list = task_list[rep].getElementsByTagName('start')
        for rep_start in range(0, len(start_list)):
            start = start_list[rep_start].attributes['dateTime'].value
        end_list = task_list[rep].getElementsByTagName('end')
        dueDate = []
        for rep_end in range(0, len(end_list)):
            end = end_list[rep_end].attributes['dateTime'].value
            # dueDate = end_list[rep_end].attributes['dueDateTime'].value
            dueDate = end_list[rep_end].getAttribute('dueDateTime')
        inputMaterials = task_list[rep].getElementsByTagName('inputMaterial')
        materialREF =[]
        quantity =[]
        storageREF=[]
        for rep_inMat in range(0, len(inputMaterials)):
            materialREF = inputMaterials[rep_inMat].attributes['materialREF'].value
            quantity = inputMaterials[rep_inMat].attributes['quantity'].value
            storageREF = inputMaterials[rep_inMat].attributes['storageREF'].value
        category = task_list[rep].getElementsByTagName('category')
        categoryREF = []
        for rep_cat in range(0, len(category)):
            categoryREF = category[rep_cat].attributes['categoryREF'].value
        listOfDetails.append(
            [task_ID, wo_ID, op_ID, start, end, dueDate, materialREF, quantity, storageREF, categoryREF])

    df = pd.DataFrame(listOfDetails)
    df.columns = ['task_ID', 'wo_ID', 'op_ID', 'start', 'end', 'dueDate', 'materialREF', 'quantity', 'storageREF',
                  'categoryREF']
    return df

def simpleMaterialBasedGantt(df,name):

    fig = px.timeline(df, x_start="start", x_end="end", y="materialREF", color="wo_ID",
                      color_discrete_sequence=px.colors.qualitative.Light24)
    fig.update_traces(marker=dict(line=dict(width=1, color='black')))
    fig.update_layout(title="Material Gantt Chart "+name, showlegend=True, height=700, width=1000, font=dict(
        family="Courier New, monospace",
        size=8,
        color="RebeccaPurple"
    ))

    #fig.show()
    return fig

def resourceDetails(xmldoc):

    listOfDetails = []
    category_list = xmldoc.getElementsByTagName('category')
    for rep_cat in range(0, len(category_list)):
        if (category_list[rep_cat].childNodes):
            requirement = category_list[rep_cat].getElementsByTagName('requirement')
            category_ID = category_list[rep_cat].attributes['ID'].value
        for rep_req in range(0, len(requirement)):
            resourceREF = requirement[rep_req].attributes['resourceREF'].value
            listOfDetails.append([category_ID, resourceREF])

    res_df = pd.DataFrame(listOfDetails)
    res_df.columns = ['categoryREF', 'resourceREF']
    res_df

    return res_df

def detailedResourceBasedGantt(df,res_df, name):

    df_merge_res = df.merge(res_df, on="categoryREF")
    df_merge_res['start'] = df_merge_res['start'].astype('datetime64')
    df_merge_res['end'] = df_merge_res['end'].astype('datetime64')
    df_merge_res['dueDate'] = df_merge_res['dueDate'].astype('datetime64')

    fig = px.timeline(df_merge_res, x_start="start", x_end="end", y="resourceREF", color="wo_ID",
                      color_discrete_sequence=px.colors.qualitative.Light24, hover_name="task_ID",
                      opacity=0.5
                      )
    dia = px.scatter(df_merge_res, x="dueDate", y="resourceREF", color="wo_ID", symbol_sequence=['circle'],
                     color_discrete_sequence=px.colors.qualitative.Light24, hover_name="task_ID",
                     # opacity=0.5
                     )

    arr = px.timeline(df_merge_res, x_start="dueDate", x_end="end", y="resourceREF", color="wo_ID",
                      color_discrete_sequence=px.colors.qualitative.Light24, hover_name="dueDate"
                      , opacity=0.5
                      )

    fig.update_traces(marker=dict(line=dict(width=0.25, color='black')))
    dia.update_traces(marker=dict(size=7, line=dict(width=0.5, color='black')))
    arr.update_traces(width=0.1)

    fig.update_layout(title="Resource Gantt Chart "+name, showlegend=True, height=700, width=1000, font=dict(
        family="Courier New, monospace",
        size=8,
        color="RebeccaPurple"
    ))

    new_fig = go.Figure(data=dia.data + fig.data + arr.data, layout=fig.layout)
    return new_fig

def horizonDetails(xmldoc):

    listOfDetails = []
    horizon_list = xmldoc.getElementsByTagName('horizon')
    for rep in range(0, len(horizon_list)):
        timeUnit = horizon_list[rep].attributes['timeUnit'].value
        earliestStart = horizon_list[rep].attributes['earliestStart'].value
        latestEnd = horizon_list[rep].attributes['latestEnd'].value
        listOfDetails.append([earliestStart, latestEnd, timeUnit])

    horizon_df = pd.DataFrame(listOfDetails)
    horizon_df.columns = ['earliestStart', 'latestEnd', 'timeUnit']
    return earliestStart

def storageDetails(xmldoc, earliestStart):

    listOfDetails = []
    storage_list = xmldoc.getElementsByTagName('storage')
    for rep in range(0, len(storage_list)):
        storage_ID = storage_list[rep].attributes['ID'].value
        inStocks = storage_list[rep].getElementsByTagName('inStock')
        for rep_sch in range(0, len(inStocks)):
            materialREF = inStocks[rep_sch].attributes['materialREF'].value
            initialQuantity = inStocks[rep_sch].attributes['initialQuantity'].value

            listOfDetails.append([storage_ID, materialREF, initialQuantity, earliestStart])

    storage_df = pd.DataFrame(listOfDetails)
    storage_df.columns = ['storageREF', 'materialREF', 'quantity', 'time']
    storage_df['quantity'] = pd.to_numeric(storage_df['quantity'])
    storage_df

    return storage_df

def outputMaterialDetails(xmldoc):

    listOfDetails = []
    task_list = xmldoc.getElementsByTagName('task')
    for rep in range(0, len(task_list)):
        task_ID = task_list[rep].attributes['ID'].value
        outputMaterials = []
        outputMaterials = task_list[rep].getElementsByTagName('outputMaterial')
        start_list = task_list[rep].getElementsByTagName('start')
        for rep_start in range(0, len(start_list)):
            start = start_list[rep_start].attributes['dateTime'].value
        end_list = task_list[rep].getElementsByTagName('end')
        for rep_end in range(0, len(end_list)):
            end = end_list[rep_end].attributes['dateTime'].value
        materialREF=[]
        quantity=[]
        storageREF = []
        for rep_outMat in range(0, len(outputMaterials)):
            materialREF = outputMaterials[rep_outMat].attributes['materialREF'].value
            quantity = outputMaterials[rep_outMat].attributes['quantity'].value
            storageREF = outputMaterials[rep_outMat].attributes['storageREF'].value
            listOfDetails.append([task_ID, materialREF, quantity, storageREF, end])

    outMat_df = pd.DataFrame(listOfDetails)
    outMat_df.columns = ['task_ID', 'materialREF', 'quantity', 'storageREF', 'time']
    outMat_df['quantity'] = pd.to_numeric(outMat_df['quantity'])
    return outMat_df

def purchaseDetails(xmldoc):

    listOfDetails = []
    purchase_list = xmldoc.getElementsByTagName('purchaseOrder')
    for rep in range(0, len(purchase_list)):
        materialREF = purchase_list[rep].attributes['materialREF'].value
        storageREF = purchase_list[rep].attributes['storageREF'].value
        quantity = purchase_list[rep].attributes['quantity'].value
        time = purchase_list[rep].attributes['time'].value
        state = purchase_list[rep].attributes['state'].value
        listOfDetails.append([materialREF, storageREF, quantity, time, state])

    purchase_df = pd.DataFrame(listOfDetails)
    purchase_df.columns = ['materialREF', 'storageREF', 'quantity', 'time', 'state']
    purchase_df['quantity'] = pd.to_numeric(purchase_df['quantity'])
    return purchase_df

def inputMaterialDetails(xmldoc):

    listOfDetails = []
    task_list = xmldoc.getElementsByTagName('task')
    for rep in range(0, len(task_list)):
        task_ID = task_list[rep].attributes['ID'].value
        inputMaterials = []
        inputMaterials = task_list[rep].getElementsByTagName('inputMaterial')
        start_list = task_list[rep].getElementsByTagName('start')
        for rep_start in range(0, len(start_list)):
            start = start_list[rep_start].attributes['dateTime'].value
        end_list = task_list[rep].getElementsByTagName('end')
        for rep_end in range(0, len(end_list)):
            end = end_list[rep_end].attributes['dateTime'].value
        materialREF = []
        quantity = []
        storageREF = []
        for rep_inMat in range(0, len(inputMaterials)):
            materialREF = inputMaterials[rep_inMat].attributes['materialREF'].value
            quantity = inputMaterials[rep_inMat].attributes['quantity'].value
            storageREF = inputMaterials[rep_inMat].attributes['storageREF'].value
            listOfDetails.append([task_ID, materialREF, quantity, storageREF, start])

    inMat_df = pd.DataFrame(listOfDetails)
    inMat_df.columns = ['task_ID', 'materialREF', 'quantity', 'storageREF', 'time']
    inMat_df['quantity'] = -1 * pd.to_numeric(inMat_df['quantity'])
    return inMat_df

def materialAvailabilityChart(inMat_df, outMat_df, purchase_df, storage_df, name):

    appendDf = inMat_df.append(outMat_df, ignore_index=True)
    appendDf = purchase_df.append(appendDf, ignore_index=True)
    appendDf = storage_df.append(appendDf, ignore_index=True)
    appendDf['time'] = pd.to_datetime(appendDf['time'])
    appendDf.sort_values(['time'], inplace=True, ascending=[True])
    appendDf = appendDf.groupby(['time', 'materialREF'], as_index=False)["quantity"].sum()

    pivot = pd.pivot_table(appendDf, values="quantity", index=["time"], columns=["materialREF"])
    pivot = pivot.cumsum()

    fig, axes = plt.subplots(nrows=11, ncols=3, figsize=(20, 200))

    k = 0
    j = 0
    for i in range(len(pivot.columns)):
        pivot[pivot[pivot.columns[i]].notna()][pivot.columns[i]].plot.bar(ax=axes[k, j]);
        axes[k, j].set_title(pivot.columns[i])
        if (j == 2):
            j = 0
            k += 1
        else:
            j += 1

    #plt.savefig("images/"+name+".pdf", dpi=150)

    return plt



if __name__ == '__main__':

    path = "scenarios\*.xml"

    for file_rep in glob.glob(path):
        xmldoc = minidom.parse(file_rep)
        filename = os.path.basename(file_rep)
        filename = filename.replace('.xml', '')
        print(filename)

        tasks = taskDetails(xmldoc)
        materialGantt = simpleMaterialBasedGantt(tasks, filename )
        plotly.offline.plot(materialGantt, filename='images\Material_gantt_' + filename + '.html')

        resources = resourceDetails(xmldoc)
        resouceGantt = detailedResourceBasedGantt(tasks,resources, filename)
        plotly.offline.plot(resouceGantt, filename='images\Resource_gantt_' + filename+ '.html')

        earliestStart = horizonDetails(xmldoc)
        storages = storageDetails(xmldoc , earliestStart)
        outputMaterials = outputMaterialDetails(xmldoc)
        purchaseOrders = purchaseDetails(xmldoc)
        inputMaterials = inputMaterialDetails(xmldoc)
        materailShortagePlot=materialAvailabilityChart(inputMaterials,
                                  outputMaterials,
                                  purchaseOrders,
                                  storages,
                                  filename)

        materailShortagePlot.savefig('images\materialShortage_' + filename+ '.pdf', dpi=150)
